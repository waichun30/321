package com.example.demo;

import java.util.logging.Logger;
import com.example.demo.model.CustomerTable;
import com.example.demo.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.hibernate.validator.internal.util.logging.Log;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
//@WebAppConfiguration
//@AutoConfigureMockMvc
//@RunWith(SpringJUnit4ClassRunner.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerControllerTest {

    Long id = 26l;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private CustomerController customerController;
    @Mock
    private CustomerTable customerTable;


//    @Test
//    public void test1() throws Exception {
//        CustomerTable customerTable = customerController.getUser(2l);
//        assertEquals("Ken", customerTable.getName());
//
//    }

    @Test
    public void a_test_post() throws Exception {

        System.out.println("hello");
        try{
            CustomerTable customer = new CustomerTable();
            customer.setId(id);
            customer.setName("test5");
            customer.setAge(44);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
            String request_json = ow.writeValueAsString(customer);

            mvc.perform(post("/customer/add2")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(request_json))
                    .andExpect(status().is2xxSuccessful())
                    .andDo(print());


            System.out.println(request_json);

        }catch (java.lang.Exception ex){
            ex.printStackTrace();
        }

    }

//    @Test
//    public void b_test_get() throws java.lang.Exception {
//
//        try {
//            mvc.perform(get("http://localhost:8080/customer/2")
//                    .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_UTF8))
//                    .andExpect(status().isOk())
//                    .andExpect(content().string(CoreMatchers.containsString("{\"id\":2,\"name\":\"Ken\",\"age\":25}")))
////                .andExpect(jsonPath("$.id").value(id))
////                .andExpect(jsonPath("$.name").value("Test"))
////                .andExpect(jsonPath("$.age").value(44))
//                    .andDo(print());
//
//        }catch(java.lang.Exception ex){
//            ex.printStackTrace();
//        }



//    }
//
//    @Test
//    public void c_test_put() throws java.lang.Exception {
//        mvc.perform(put("http://localhost:8080/customer/alter/"+id)
//                .accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.age").value(45))
//                .andDo(MockMvcResultHandlers.print());
//    }
//
//    @Test
//    public void d_test_delete() throws java.lang.Exception {
//        mvc.perform(delete("http://localhost:8080/customer/delete/"+id)
//                .accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andDo(print());
//    }
}