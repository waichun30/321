/*
 * Controller file
 * Manage method
 *
 * */


package com.example.demo;

import com.example.demo.repository.CustomerRepository;
import com.example.demo.model.CustomerTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Resource
    @Autowired
    private CustomerRepository customerRep;

    public CustomerController() {

    }

    // GET method to retrieve all data
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<CustomerTable> getAll() {
        return customerRep.findAll();
    }

    //Second API
    // Get Method to retrieve specific data
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public CustomerTable getUser(@PathVariable("id") Long id) {
        return customerRep.findById(id).orElseThrow(() -> new Exception("Customer", "id", id));
    }

    //
    // POST method, save name and age to database
    @RequestMapping(value = "/add/{name},{age}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> addCustomer(@PathVariable("name") String name, @PathVariable("age") int age) {
        CustomerTable customerTable = new CustomerTable(name, age);
        customerRep.save(customerTable);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    //First API
    // POST method, save name and age to database
    @RequestMapping(value = "/add2", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> addCustomer2(@RequestBody CustomerTable c) {
        //CustomerTable customerTable = new CustomerTable(name, age);
        customerRep.save(c);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


    //Third API
    // PUT method, alter/update customer age +1 and save
    @RequestMapping(value = "/alter/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public CustomerTable alterAge(@PathVariable("id") Long id) {
        CustomerTable customerTable = customerRep.findById(id)
                .orElseThrow(() -> new Exception("Customer", "id", id));
        int newage = customerTable.getAge() + 1;
        customerTable.setAge(newage);
        CustomerTable updatedcustomerTable = customerRep.save(customerTable);
        return updatedcustomerTable;
    }

    //Forth API
    // Delete method, delete the data based on customer id and save it
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public List<CustomerTable> deleteCustomer(@PathVariable("id") Long id) {
        CustomerTable customerTable = customerRep.findById(id)
                .orElseThrow(() -> new Exception("Customer", "id", id));
        customerRep.delete(customerTable);
        return customerRep.findAll();
    }

}